import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GraphTempPage } from './graph-temp';

@NgModule({
  declarations: [
    GraphTempPage,
  ],
  imports: [
    IonicPageModule.forChild(GraphTempPage),
  ],
})
export class GraphTempPageModule {}
