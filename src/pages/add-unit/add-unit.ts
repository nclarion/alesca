import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { AddingprocessPage } from '../addingprocess/addingprocess';
import { BLE } from '@ionic-native/ble';

/**
 * Generated class for the AddUnitPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-unit',
  templateUrl: 'add-unit.html',
})
export class AddUnitPage {

  availabledevice : any;
  bleundefined : any;
  devices: any[] = [];
  statusMessage: string;
  isScanning : any;
  nav : any;
  peripheral: any = {};

  constructor(public navCtrl: NavController, 
              public navParams: NavParams, 
              public ble: BLE, 
              private toastCtrl: ToastController,
              private ngZone: NgZone) {
  }

  ionViewDidLoad() {
    this.startScanning();
  }

  startScanning(){

    this.setStatus('Scanning for Bluetooth LE Devices');
    this.devices = [];  // clear list

    this.ble.scan([], 5).subscribe(
      device => this.onDeviceDiscovered(device), 
      error => this.scanError(error)
    );

    setTimeout(() => {

      this.ble.stopScan().then(() => {
        this.setStatus('Scan complete');
        this.isScanning = false;
      });

    }, 5000, );

  };

  onDeviceDiscovered(device) {
    this.ngZone.run(() => {
      this.devices.push(device);
    });
  }

  // If location permission is denied, you'll end up here
  scanError(error) {
    this.setStatus('Error ' + error);
    let toast = this.toastCtrl.create({
      message: 'Error scanning for Bluetooth low energy devices',
      position: 'middle',
      duration: 5000
    });
    toast.present();
  }

  // onDeviceDisconnected(peripheral) {
  //   let toast = this.toastCtrl.create({
  //     message: 'The peripheral unexpectedly disconnected',
  //     duration: 3000,
  //     position: 'middle'
  //   });
  //   toast.present();
  // }

  // Disconnect peripheral when leaving the page
  // ionViewWillLeave() {
  //   this.ble.disconnect(this.peripheral.id).then(
  //     () => alert('Disconnected ' + JSON.stringify(this.peripheral)),
  //     () => alert('ERROR disconnecting ' + JSON.stringify(this.peripheral))
  //   )
  // }

  setStatus(message) {
    console.log(message);
    this.ngZone.run(() => {
      this.statusMessage = message;
    });
  }

  deviceSelected(device) {
    // alert(JSON.stringify(device) + ' selected');
    this.navCtrl.push(AddingprocessPage, {
      device: device
    });
  }

}
