import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the RecipePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-recipe',
  templateUrl: 'recipe.html',
})
export class RecipePage {
	
	id : string;
	name : string;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

 	 this.id = navParams.get('id');
   this.name = navParams.get('name');

  }
  todo = {}
  logForm() {
    console.log(this.todo)
  }
}
