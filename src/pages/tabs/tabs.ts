import { Component } from '@angular/core';

import { CalibrationPage } from '../calibration/calibration';
import { SettingsPage } from '../controls/controls';
import { Wifipage } from '../wifidashboard/wifidashboard';
import { homedashboard } from '../homedashboard/homedashboard';
import { StartupPage } from '../startup/startup';

@Component({
  selector: 'tabs-page',
  templateUrl: 'tabs.html'
})
export class TabsPage {

  startup = StartupPage;
  wifidashboard = Wifipage;
  calibration = CalibrationPage;
  homesettings = SettingsPage;
  homedevice = homedashboard;

  constructor() {

  };
  
}
