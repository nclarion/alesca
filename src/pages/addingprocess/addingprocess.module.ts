import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddingprocessPage } from './addingprocess';

@NgModule({
  declarations: [
    AddingprocessPage,
  ],
  imports: [
    IonicPageModule.forChild(AddingprocessPage),
  ],
})
export class AddingprocessPageModule {}
