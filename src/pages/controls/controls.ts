import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ControlsSetPage } from '../controls-set/controls-set';

@Component({
  selector: 'page-settings',
  templateUrl: 'controls.html'
})
export class SettingsPage {

	ports = {

		port1 : false,
		port2 : false,
		port3 : false,
		port4 : false,
		port5 : false,
		port6 : false,
		port7 : false,
		port8 : false

	};

  constructor(public navCtrl: NavController) {

  }

  goset(){
    // push another page onto the navigation stack
    // causing the nav controller to transition to the new page
    // optional data can also be passed to the pushed page.
    this.navCtrl.push(ControlsSetPage, {
      id: "123",
      name: "Carl"
    });

  }

}
