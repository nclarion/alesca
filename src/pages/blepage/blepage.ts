import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BLE } from '@ionic-native/ble';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
  providers : [BLE]
})
export class BLEPage {

availabledevice : any;
bleundefined : any;
devices : any;
isScanning : any;
nav : any;

  constructor(public navCtrl: NavController, public ble: BLE) {

	this.devices = [];
	this.isScanning = false;

	this.ble.scan([], 5).subscribe(device => {
      this.availabledevice = JSON.stringify(device);
    }, error => {
      this.bleundefined = error;
    });

  }

  startScanning(){

	alert('“Scanning Started”');
	this.devices = [];
	this.isScanning = true;
	this.ble.scan([], 5).subscribe(device => {

		this.devices.push(device);
		this.isScanning = false;

	});

	setTimeout(() => {
		this.ble.stopScan().then(() => {
		alert('scanning has stopped');
		alert(JSON.stringify(this.devices))
		this.isScanning = false;
		});
	}, 3000);

  };

  connectToDevice(device) {
	alert(JSON.stringify(device));
	alert('‘Connect To Device’');
	alert(JSON.stringify(device))
	this.nav.push(device, {
	device: device
	});

  }

}
