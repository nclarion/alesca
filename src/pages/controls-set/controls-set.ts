import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the SettingSetPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-setting-set',
  templateUrl: 'controls-set.html',
})

export class ControlsSetPage {

id : string;
name : string;
private selectOption: any;

inquirybodies = {
	companyname : '',
	fullname : '',
	emailaddress: '',
	inquirydesc : '',
	telno : '',
	agree : '',
	toEmail : '',
	departmentname : '',
	title: '',
	port1 : {
		id : 3
	},
	port2 : {
		id : 4
	}
 }

  constructor(public navCtrl: NavController, public navParams: NavParams) {

 	 this.id = navParams.get('id');
     this.name = navParams.get('name');

     this.selectOption = [

	      {
	        id: 1,
	        label: "OFF",
	        value: 0
	      }, {
	        id: 2,
	        label: "ON",
	        value: 1
	      }, 
			{
	        id: 3,
	        label: "Cycling",
	        value: 2
	      },
	      {
	        id: 4,
	        label: "Schedule",
	        value: 3
	      }

     ];

    this.inquirybodies.port1 = this.selectOption[2];
    this.inquirybodies.port2 = this.selectOption[3];

  }


}
