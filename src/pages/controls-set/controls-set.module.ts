import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ControlsSetPage } from './controls-set';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(ControlsSetPage),
  ],
})
export class SamplePageModule {}
