import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CalibrationPage } from './calibration';

@NgModule({
  declarations: [
    CalibrationPage,
  ],
  imports: [
    IonicPageModule.forChild(CalibrationPage),
  ],
})
export class CalibrationPageModule {}
