import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RecipePage } from '../recipe/recipe';
import { GraphTempPage } from '../graph-temp/graph-temp';
import { TabsPage } from '../tabs/tabs';
/**
 * Generated class for the DevicesPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-devices',
  templateUrl: 'homedashboard.html'
})

export class homedashboard {
  tabpage:any = TabsPage;
	device : any;
	connecting : any;
	characteristics : any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {



  }

  todo = {}
  logForm() {
    console.log(this.todo)
  }

  settings(){
    this.navCtrl.push(RecipePage, {
      id: "123",
      name: "Carl"
    });

  }

  viewgraph(){

    this.navCtrl.push(GraphTempPage, {
      id: "123",
      name: "Carl"
    });

  }
}
