import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { homedashboard } from './homedashboard';

@NgModule({
  declarations: [
  ],
  imports: [
    IonicPageModule.forChild(homedashboard),
  ],
})
export class SamplePageModule {}
