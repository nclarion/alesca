import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AddUnitPage } from '../add-unit/add-unit';
import { BLE } from '@ionic-native/ble';
import { homedashboard } from '../homedashboard/homedashboard';
import { ToastController } from 'ionic-angular';

/**
 * Generated class for the StartupPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-startup',
  templateUrl: 'startup.html',
})
export class StartupPage {

  availabledevice : any;
  bleundefined : any;
  devices : any;
  statusMessage: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, public ble: BLE, private ngZone: NgZone, private toastCtrl: ToastController) {

  }

  startScanning(){


    this.devices = [];  // clear list

    this.ble.scan([], 5).subscribe(
      device => this.onDeviceDiscovered(device), 
      error => this.scanError(error)
    );

    setTimeout(() => {

      this.ble.stopScan().then(() => {
        this.setStatus('Scan complete');

      });

    }, 5000);

  };

  // If location permission is denied, you'll end up here
  scanError(error) {
    this.setStatus('Error ' + error);
    let toast = this.toastCtrl.create({
      message: 'Error scanning for Bluetooth low energy devices',
      position: 'middle',
      duration: 20000
    });
    toast.present();
  }

  ionViewDidLoad() {
    this.startScanning();
  }

  onDeviceDiscovered(device) {
    this.ngZone.run(() => {
      this.devices.push(device);
    });
  }
  setStatus(message) {
    this.ngZone.run(() => {
      this.statusMessage = message;
    });
  }

  addunit(){
    this.navCtrl.push(AddUnitPage, {

    });
  }

}
