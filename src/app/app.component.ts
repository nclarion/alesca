import { Component, ViewChild } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Nav } from 'ionic-angular';
import { TabsPage } from '../pages/tabs/tabs';

import { CalibrationPage } from '../pages/calibration/calibration';
import { SettingsPage } from '../pages/controls/controls';
import { Wifipage } from '../pages/wifidashboard/wifidashboard';
import { homedashboard } from '../pages/homedashboard/homedashboard';

@Component({
  templateUrl: 'app.html',
})
export class MyApp {
@ViewChild(Nav) nav: Nav;

  rootPage:any = TabsPage;
  private pages : any;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });

    this.pages = [
      { title: 'Calibration', component: CalibrationPage },
      { title: 'Controls', component: SettingsPage },
      { title: 'WIFI', component: Wifipage },
      { title: 'Dashboard', component: homedashboard }
    ];

  }

  // openPage(page) {
  //   console.log(page)
  //   // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
  //   this.nav.setRoot(page.component);
  // }

}
