import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { CalibrationPage } from '../pages/calibration/calibration';

import { BLEPage } from '../pages/blepage/blepage';
import { SettingsPage } from '../pages/controls/controls';
import { Wifipage } from '../pages/wifidashboard/wifidashboard';
import { homedashboard } from '../pages/homedashboard/homedashboard';
import { TabsPage } from '../pages/tabs/tabs';
import { ControlsSetPage } from '../pages/controls-set/controls-set';
import { RecipePage } from '../pages/recipe/recipe';
import { GraphTempPage } from '../pages/graph-temp/graph-temp';
import { StartupPage } from '../pages/startup/startup';
import { AddUnitPage } from '../pages/add-unit/add-unit';
import { AddingprocessPage } from '../pages/addingprocess/addingprocess';

/src/
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { BLE } from '@ionic-native/ble';

@NgModule({
  declarations: [
    MyApp,
    BLEPage,
    SettingsPage,
    Wifipage,
    TabsPage,
    homedashboard,
    ControlsSetPage,
    CalibrationPage,
    RecipePage,
    GraphTempPage,
    StartupPage,
    AddUnitPage,
    AddingprocessPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    BLEPage,
    SettingsPage,
    Wifipage,
    TabsPage,
    homedashboard,
    ControlsSetPage,
    CalibrationPage,
    RecipePage,
    GraphTempPage,
    StartupPage,
    AddUnitPage,
    AddingprocessPage
  ],
  providers: [
    StatusBar,
    BLE,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
