Alesca Template

## How to use this template

### pre·req·ui·site
[Android SDK](https://developer.android.com/studio/index.html).
[Nodejs](https://nodejs.org/en/).
[ionic cli](http://ionicframework.com/docs/v1/guide/installation.html/).

```bash
$ git clone https://gitlab.com/nclarion/alesca.git
$ cd alesca && npm install or yarn install
```

```bash
$ ionic cordova platform add android
$ ionic cordova platform build android
$ ionic cordova run android
```

Substitute ios for android if not on a Mac.

